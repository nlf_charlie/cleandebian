# Clean debian/jessie64 w/buildkit
Includes LAMP stack, sublime-like editor "slap", phpmyadmin, configures GIT and SSH.

### Purpose
The purpose of using this VM (as opposed to "civicrm-buildkit-vagrant") is to match civihosting production server o/s and lamp stack.

### How do I get set up?
* Place your private SSH key beside Vagrantfile and update the Vagrantfile with the name of your key.
* Other variables in Vagrantfile should be updated like git email and name.

### Who do I talk to?
* carlos@noahslightfoundation.org

### Notes
* You must manually manage /etc/hosts on your host machine (while creating site instances via civibuild)