#!/bin/bash

echo -e " _           _ _     _ _    _ _   "
echo -e "| |__  _   _(_) | __| | | _(_) |_ "
echo -e "| '_ \| | | | | |/ _' | |/ / | __|"
echo -e "| |_) | |_| | | | (_| |   <| | |_ "
echo -e "|_.__/ \__,_|_|_|\__,_|_|\_\_|\__|"

whoami
echo -e "\e[34m=====> Updating PATH for buildkit"
echo "
export PATH=\"${BUILDKIT_DIR}/bin:\$PATH\"
alias bb='cd ${BUILDKIT_DIR}'" | sudo tee --append $USER_HOME.bashrc > /dev/null

echo -e "\e[34m=====> Installing nodejs5"
curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
sudo apt-get install -y nodejs > /dev/null
#nodejs includes npm

echo -e "\e[34m=====> Installing slap"
sudo npm install -s -g slap@latest > /dev/null
# echo "
# [fileBrowser]
# width=30" | sudo tee $USER_HOME.slap/config > /dev/null


echo -e "\e[34m=====> Downloading buildkit"
git clone https://github.com/civicrm/civicrm-buildkit.git $BUILDKIT_DIR/ > /dev/null

echo -e "\e[34m=====> Setting up buildkit"
$BUILDKIT_DIR/bin/civi-download-tools > /dev/null 2>&1

echo -e "\e[34m=====> Configure amp"
# calling amp with full path because vagrant doesnt allow source .bashrc
# during provision and thus doesnt update the path with the buildkit
$BUILDKIT_DIR/bin/amp config:set --db_type=mysql_dsn
$BUILDKIT_DIR/bin/amp config:set --mysql_dsn='mysql://root:root@127.0.0.1:3306'
$BUILDKIT_DIR/bin/amp config:set --perm_type=linuxAcl
$BUILDKIT_DIR/bin/amp config:set --perm_user=vagrant
$BUILDKIT_DIR/bin/amp config:set --hosts_type=none
$BUILDKIT_DIR/bin/amp config:set --httpd_type=apache24
$BUILDKIT_DIR/bin/amp config:set --httpd_visibility=all
$BUILDKIT_DIR/bin/amp config:set --httpd_restart_command='sudo service apache2 restart'

echo -e "\e[34m=====> Updating Apache for Buildkit"
echo "
EnableSendfile Off
IncludeOptional ${USER_HOME}.amp/apache.d/*.conf" | sudo tee --append /etc/apache2/apache2.conf > /dev/null
sudo service apache2 restart