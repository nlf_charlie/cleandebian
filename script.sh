#!/bin/bash

echo -e "\e[45m               d8b        ,d8888b"
echo -e "\e[45m               88P        88P'   "
echo -e "\e[45m              d88      d888888P  "
echo -e "\e[45m  88bd88b     888        ?88'    "
echo -e "\e[45m  88P' ?8b    ?88        88P     "
echo -e "\e[45m d88   88P     88b      d88      "
echo -e "\e[45md88'   88b      88b    d88'      "

echo -e "\e[34m=====> Clean up from previous VM instance if any"
rm -rf $APACHE_LOGS_DIR
rm -rf $CLEAN_WWW

echo -e "\e[34m=====> Make Dirs"
mkdir -p $APACHE_LOGS_DIR
mkdir -p $CLEAN_WWW


echo -e "\e[34m=====> Adding specific mysql repo"
# sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5072E1F5
sudo apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 5072E1F5 > /dev/null
echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-5.7" | sudo tee /etc/apt/sources.list.d/mysql.list > /dev/null

echo -e "\e[34m=====> Updating apt-get"
sudo apt-get update > /dev/null



echo -e "\e[34m=====> Installing GIT and tools"
sudo apt-get -y install vim git zip curl wget > /dev/null

echo -e "\e[34m=====> Setting default git user"
echo "[user]
	email = ${GIT_EMAIL}
	name = ${GIT_NAME}
[core]
	excludesfile = ${USER_HOME}.gitignore_global" | sudo tee $USER_HOME.gitconfig > /dev/null
chown vagrant:vagrant $USER_HOME.gitconfig


echo -e "\e[34m=====> writing a .gitignore_global"
echo '
### macOS ###
*.DS_Store
.AppleDouble
.LSOverride

# Thumbnails
._*

# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk
'  | sudo tee $USER_HOME.gitignore_global > /dev/null
chown vagrant:vagrant $USER_HOME.gitignore_global





echo -e "\e[34m=====> Installing PHP & Apache"
sudo apt-get -y install php5 php5-mcrypt php5-imap php5-curl php5-cli php5-mysql php5-gd php5-intl php5-xsl libapache2-mod-php5 php5-dev php-pear apache2 apache2.2-common > /dev/null

sudo a2enmod rewrite > /dev/null
sudo service apache2 restart > /dev/null

echo -e "\e[34m=====> apt-cache policy apache2"
apt-cache policy apache2

echo -e "\e[34m=====> php -v"
php -v


echo -e "\e[34m=====> Installing Mysql"
echo "mysql-community-server mysql-community-server/re-root-pass password root" | sudo debconf-set-selections
echo "mysql-community-server mysql-community-server/root-pass password root" | sudo debconf-set-selections
sudo apt-get -y install mysql-server > /dev/null



# creates db in case needed outside of buildkit
echo -e "\e[34m=====> creating blank db"
mysql -uroot -proot -e "CREATE DATABASE ${DB_NAME};FLUSH PRIVILEGES;"



echo -e "\e[34m=====> Updating mysql conf"
echo '
[mysqld]
sql_mode=NO_ENGINE_SUBSTITUTION
' | sudo tee --append /etc/mysql/my.cnf > /dev/null
sudo service mysql restart











echo -e "\e[34m=====> Configure Apache"
echo '
ServerName localhost
' | sudo tee --append /etc/apache2/apache2.conf > /dev/null

echo "<VirtualHost *:80>
	ServerName ${VM_HOST_NAME}
	ServerAlias ${VM_HOST_NAME_SHORT}
	ServerAdmin webmaster@localhost
	DocumentRoot ${CLEAN_WWW}

	ErrorLog ${APACHE_LOGS_DIR}error.log
	CustomLog ${APACHE_LOGS_DIR}access.log combined

	<Directory ${CLEAN_WWW}>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
	</Directory>
</VirtualHost>" | sudo tee /etc/apache2/sites-available/$VM_HOST_NAME_SHORT.conf > /dev/null


# prevent redirection to ISP, this ip must match that specified in the VagrantFile
echo -e "\e[34m=====> modifying /etc/hosts"
echo "
${VM_PRIVATE_IP} ${VM_HOST_NAME}
${VM_PRIVATE_IP} ${VM_HOST_NAME_SHORT}
" | sudo tee /etc/hosts > /dev/null


echo -e "\e[34m=====> Restart Apache"
sudo a2ensite $VM_HOST_NAME_SHORT.conf > /dev/null
sudo service apache2 restart > /dev/null





# mysql-server must first be installed for the replace cmd
echo -e "\e[34m=====> Change apache user to vagrant"
sudo replace "export APACHE_RUN_USER=www-data" "export APACHE_RUN_USER=vagrant" -- /etc/apache2/envvars
sudo replace "export APACHE_RUN_GROUP=www-data" "export APACHE_RUN_GROUP=vagrant" -- /etc/apache2/envvars










echo -e "\e[34m=====> Configure SSH"
# Implement SSH keys for bitbucket
if [ ! -d /root/.ssh ]
	then mkdir -p /root/.ssh
fi
if [ ! -d $USER_HOME.ssh ]
	then mkdir -p $USER_HOME.ssh
fi
sudo cp $VMROOT$PRIVATE_KEY /root/.ssh/bitbucket_nlf
sudo cp $VMROOT$PRIVATE_KEY $USER_HOME.ssh/bitbucket_nlf

echo "Host bitbucket.org
 IdentitiesOnly yes
 StrictHostKeyChecking no
 IdentityFile /root/.ssh/${PRIVATE_KEY}" | sudo tee /root/.ssh/config > /dev/null

echo "Host bitbucket.org
 IdentitiesOnly yes
 StrictHostKeyChecking no
 IdentityFile ${USER_HOME}${PRIVATE_KEY}" | sudo tee $USER_HOME.ssh/config > /dev/null






echo -e "\e[34m=====> Install PHPMYADMIN"

echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password root" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password root" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password root" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | sudo debconf-set-selections

sudo apt-get -y install phpmyadmin > /dev/null
sudo php5enmod mcrypt











echo -e "\e[34m=====> Misc Bash"
sudo replace "#force_color_prompt=yes" "force_color_prompt=yes" -- $USER_HOME.bashrc

# echo "" | sudo tee --append $USER_HOME.bashrc > /dev/null

echo -e "\e[34m=====> Restart Apache"
sudo service apache2 restart





echo -e "\e[44m   ,d8888b  d8,          "
echo -e "\e[44m   88P'    \`8P           "
echo -e "\e[44md888888P                 "
echo -e "\e[44m  ?88'      88b  88bd88b "
echo -e "\e[44m  88P       88P  88P' ?8b"
echo -e "\e[44m d88       d88  d88   88P"
echo -e "\e[44md88'      d88' d88'   88b"